$(document).ready(function() {

    // Array to store data from form
    var info = [];
    info.length = 4;
    var fname = false;
    var lname = false;
    var sex = false;


    // Method used to check if there are three elements in the array
    var checkNumberElements = function() {
        if(fname && lname && sex) {

            $('form').append('<br>' +
                             '<label>How did you arrive ?</label>' +
                             '<select id="transport">' +
                                '<option>By bus</option>' +
                                '<option>By train</option>' +
                             '</select>');

            // Method called when the transport selection is changing
            $('#transport').on('change', function() {
                info[3] = $(this).val();
                if(checkValues()) {
                    $('#here').append(info[0] + ',' + info[1] + ',' + info[2] + ',' + info[3]);
                }
            });
        }
    };


    // Method used to check if values of array are not null
    var checkValues = function() {

        var complete = true;

        $.each(info, function(i, val) {
            if(val === "") {
                complete = false;
            }
        });

        return complete;
    };


    // Method called when inputs gain focus
    $('input').on('focus', function() {
        $('input').css('background-color', 'white');
        $(this).css('background-color', 'gray');
        fname = true;
        checkNumberElements();
    });


    // Method called when inputs lose focus
    $('input').on('blur', function() {
        info[0] = $('#ename').val();
        info[1] = $('#lname').val();
        lname = true;
        checkNumberElements();
    });


    // Method called when the sex selection is changing
    $('#selSex').on('change', function() {
        info[2] = $('#selSex').val();
        sex = true;
        checkNumberElements();
    });

});