$(document).ready(function() {

    var click = 0;
    var imgClick = 0;

    var changeImages = function() {

        // Getting images
        var img1 = $('#img1');
        var img2 = $('#img2');
        var img3 = $('#img3');

        // Displaying images
        img1.attr('src', 'red.jpg');
        img2.attr('src', 'blue.jpg');
        img3.attr('src', 'yellow.jpg');

        // Changing 'alt' attributes
        img1.attr('alt', 'red');
        img2.attr('alt', 'blue');
        img3.attr('alt', 'yellow');

    };

    // Handling click event
    $('button').on('click', function() {

        // Resetting html when second click
        $('img').show();
        if(click > 0) {
            click = 0;
            $('#selection').find('ul').empty();
        }

        // Displaying text
        $('#advice').text('select your favourite colour among the colours');
        changeImages();
        $('#sel').text('your favourite colours in order');

        if(imgClick === 0) {
            // Handling click event on one image
            $('img').on('click', function () {
                var color = $(this).attr('alt');
                $('#selection').find('ul').append('<li>' + color + '</li>');
                $(this).hide();
            });
        }

        click++;
        imgClick++;

    });

});