$(document).ready(function() {

    // Storing the choice of the user ('heads' or 'tails')
    var choice = 0;


    // Creating variables used for statistics
    var nbGames = 0;
    var won = 0;
    var lost = 0;


    // Function used to update the statistics
    var updateStatistics = function() {
        $('#games').text("Games: " + nbGames);
        $('#won').text("Won: " + won);
        $('#lost').text("Lost: " + lost);
        var ratio;

        if(nbGames !== 0) {
            ratio = ((won / nbGames) * 100).toFixed(2);
        } else {
            ratio = '- ';
        }

        $('#ratio').text("Winning ratio: " + ratio + "%");
    };


    // Function used to detect choice
    $('#choice').on('change', function() {
        choice = ($(this).val() === "heads") ? 0 : 1;
    });


    // Function used to detect the click on the button
    $('#buttonFlip').on('click', function() {
        var randomNumber = Math.floor(Math.random()*2);
        var image = $('#imgCoin');

        if(randomNumber === 0) {
            image.attr('src', 'heads.jpg');
        } else {
            image.attr('src', 'tails.jpg');
        }

        // Displaying result & statistics
        nbGames++;
        if(randomNumber === choice) {
            won += 1;
            $('#result').text('You Won!');
        } else {
            lost += 1;
            $('#result').text('You Lost!');
        }

        // Updating statistics
        updateStatistics();

    });


    // Initializing display for statistics
    updateStatistics();

});