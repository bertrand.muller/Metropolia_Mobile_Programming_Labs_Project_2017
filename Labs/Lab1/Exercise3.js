// Function used to get one line of seven random numbers
function getRowLotery() {

    var rowLotery = "";

    // Seven numbers per line
    for(var j = 0; j < 7; j++) {
        var randNumb = parseInt(Math.random()*39+1);
        rowLotery += randNumb + " ";
    }

    return rowLotery;

}

var numbRows = prompt("Give me the number of rows you want", "1");

for(var i = 0; i < numbRows; i++) {
    console.log(getRowLotery());
}