// Function used to verify if a value exists in an array
function verifyValueExistsInArray(arr,val) {
    return (arr.indexOf(val) !== -1);
}

// Function used to get one line of seven random numbers
function getRowLotery() {

    var randNumbers = [];

    // Seven numbers per line
    for(var i = 0; i < 7; i++) {

        var randNumb = parseInt(Math.random()*39+1);

        while(verifyValueExistsInArray(randNumbers,randNumb)) {
            randNumb = parseInt(Math.random()*39+1);
        }

        randNumbers[i] = randNumb;
    }

    // Sorting array
    randNumbers.sort(function(a, b) {
        return a-b;
    });

    return randNumbers;

}

// Asking the number of rows to the user
var numbRows = prompt("Give me the number of rows you want", "1");

// Displaying rows of random numbers
for(var i = 0; i < numbRows; i++) {

    var row = getRowLotery();
    var printedRow = "row:";

    for(var j = 0; j < row.length-2; j++) {
        printedRow += row[j] + ",";
    }

    printedRow += row[row.length-2];
    printedRow += " and " + row[row.length-1];
    console.log(printedRow);

}