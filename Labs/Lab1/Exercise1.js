// Initializing values
var myAge = 77;
var yourAge = 88;

// Printing initial values
console.log("My age is " + myAge);
console.log("Your age is " + yourAge);

// Printing a message according to values and conditions
if (myAge > yourAge) {
    console.log("I am older");
} else if (myAge < yourAge) {
    console.log("You are older");
} else {
    console.log("Same age");
}