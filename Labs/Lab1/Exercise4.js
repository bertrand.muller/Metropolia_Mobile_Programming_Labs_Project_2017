// Function used to get one line of seven random numbers
function getRowLotery() {

    var randNumb = [];

    // Seven numbers per line
    for(var i = 0; i < 7; i++) {
        randNumb[i] = parseInt(Math.random()*39+1);
    }

    // Sorting array
    randNumb.sort(function(a, b) {
        return a-b;
    });

    return randNumb;

}

// Asking the number of rows to the user
var numbRows = prompt("Give me the number of rows you want", "1");

// Displaying rows of random numbers
for(var i = 0; i < numbRows; i++) {

    var row = getRowLotery();
    var printedRow = "";

    for(var j = 0; j < row.length-1; j++) {
        printedRow += row[j] + ",";
    }

    printedRow += row[row.length-1];
    console.log(printedRow);

}