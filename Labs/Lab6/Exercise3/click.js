$(document).on("pageinit", function() {

    // Method used to detect the click on the button B
    $('#blockB').on('click', function() {

        var blockA = $('#blockA');
        var blockC = $('#blockC');

        var textBlockA = blockA.text();
        var textBlockC = blockC.text();

        // Swapping texts
        blockA.text(textBlockC);
        blockC.text(textBlockA);

    });
});