
$(document).ready(function() {


    // Creating the map in the 'mapCanvas' div
    var map = new google.maps.Map(document.getElementById('mapCanvas'), {
        center: {lat: -34.397, lng: 150.644},
        zoom: 13
    });


    // Creating an array to save the history
    var history = [];


    // Variable used to know is the user clicked at least one on 'Locate Me'
    var located = false;


    // Variable used to know if the map is hidden or not
    var hidden = false;


    // Code excuted when the user clicks on 'Locate Me'
    $("#locate").on('click', function() {
        centerMapCurrentLocation(map);
        located = true;
    });


    // Code executed when the user clicks on '+'
    $("#plus").on('click', function() {
        var mapCanvas = $("#mapCanvas");
        mapCanvas.width(mapCanvas.width() + 30);
        mapCanvas.height(mapCanvas.height() + 30);
    });


    // Code executed when the user clicks on '-'
    $("#minus").on('click', function() {
        var mapCanvas = $("#mapCanvas");
        mapCanvas.width(mapCanvas.width() - 30);
        mapCanvas.height(mapCanvas.height() - 30);
    });


    // Code executed when the user clicks on 'Save'
    $("#save").on('click', function() {
        if(located) {
            var center = map.getCenter();
            history.push(new Date() + ' : ' + center.lat() + ',' + center.lng());
        }
    });


    // Code executed when the user clicks on 'History'
    $("#history").on('click', function() {

        var text = '';
        var textHistory = $('#textHistory');
        textHistory.text('');

        for (var i = 0; i < history.length; i++) {
            textHistory.append(history[i] + '<br>');
        }

    });


    // Code executed when the user clicks on 'Hide map'
    $("#hide").on('click', function() {
        if(!hidden) {
            $("#mapCanvas").hide();
            $(this).text('Unhide map');
            hidden = !hidden;
        } else {
            $("#mapCanvas").show();
            $(this).text('Hide map');
            hidden = !hidden;
        }

    });

});


// Function executed if there is an error with the Google Map
function handleLocationError(browserHasGeolocation, infoWindow, pos) {
    infoWindow.setPosition(pos);
    infoWindow.setContent(browserHasGeolocation ?
        'Error: The Geolocation service failed.' :
        'Error: Your browser doesn\'t support geolocation.');
}


// Function used to center the map according to the current location
function centerMapCurrentLocation(map) {

    // Try HTML5 geolocation.
    if (navigator.geolocation) {
        navigator.geolocation.getCurrentPosition(function(position) {
            var pos = {
                lat: position.coords.latitude,
                lng: position.coords.longitude
            };

            var infoWindow = new google.maps.InfoWindow({
                content: pos.lat + " , " + pos.lng
            });

            map.setCenter(pos);

            var marker = new google.maps.Marker({
                position: pos,
                map: map,
                title: "Here I am!"
            });

            marker.addListener('click', function() {
                infoWindow.open(map, marker);
            });

        }, function() {
            handleLocationError(true, infoWindow, map.getCenter());
        });
    } else {
        // Browser doesn't support Geolocation
        handleLocationError(false, infoWindow, map.getCenter());
    }

}