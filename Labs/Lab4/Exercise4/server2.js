$(document).ready(function() {

    // Request from server
    var requestServer = $.ajax({
                            url: 'http://users.metropolia.fi/~harria/Tests/MyTest.json',
                            type: 'GET',
                            async: true
                        });

    requestServer.done(function(data) {
        for(var i = 0; i < data['employees'].length; i++) {
            var employee = data['employees'][i];
            $('#pServer').append(employee['firstName'] + '--' + employee['lastName'] + '<br><br>');
        }
    });


    // Request from public server Weather
    var requestWeather = $.ajax({
                            url: 'http://api.openweathermap.org/data/2.5/weather?q=Helsinki,fi&units=metric&APPID=8122e4857dd204ee1d97877fb9aa44a8',
                            type: 'GET',
                            async: true
                         });

    requestWeather.done(function(data) {
        var temp = data['main']['temp'].toFixed(1);
        $('#pWeather').text('temperature: ' + temp + '°C');
    });

});