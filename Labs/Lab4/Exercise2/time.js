$(document).ready(function() {

    // Getting variables from local storage
    var loaded = parseInt(localStorage.getItem('loaded'));
    var nbLoaded = (loaded ? loaded+1 : 1);
    localStorage.setItem('loaded', nbLoaded);

    var time = localStorage.getItem('timeStamp');
    var lastTimeStamp = (time ? time : new Date());


    // Function used when the user wants to save the timestamp
    $('#button').on('click', function() {
        localStorage.setItem('timeStamp', new Date());
    });


    // Initializing webpage
    $('#lastDate').append(lastTimeStamp);
    $('#counter').append(nbLoaded);

});