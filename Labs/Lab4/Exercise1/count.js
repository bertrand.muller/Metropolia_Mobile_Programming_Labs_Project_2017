$(document).ready(function() {

    // Initializing global variables
    var strs = ['a', 'as', 'bis', 'c', 'cis', 'def', 'sin', 'tan'];


    // Function used to display the array
    var displayArray = function() {

        var arrayString = '[';

        for(var i = 0;  i < strs.length-1; i++) {
            arrayString += '\'' + strs[i] + '\',';
        }

        arrayString += '\'' + strs[strs.length-1] + '\']';
        $('#array').append(arrayString);

    };


    // Function used to count the number of 's' in one string
    var numberCharsInString = function(str, ch) {
        return str.split(ch).length - 1;
    };


    // Function used to count the number of 's' in the array
    var numberCharsInArray = function(ch) {

        var numbChars = 0;

        for(var i = 0; i < strs.length; i++) {
            numbChars += numberCharsInString(strs[i], ch);
        }

        return numbChars;

    };


    // Initializing webpage
    displayArray();
    $('#amount').append(numberCharsInArray('s'));

});