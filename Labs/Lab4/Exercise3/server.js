$(document).ready(function() {

    // Request from server
    var requestServer = $.ajax({
                            url: 'http://users.metropolia.fi/~harria/Tests/MyText.txt',
                            type: 'GET',
                            async: true
                        });

    requestServer.done(function(data) {
        $('#pServer').append(data);
    });


    // Request from public server Weather
    var requestWeather = $.ajax({
                            url: 'http://api.openweathermap.org/data/2.5/forecast?id=524901&APPID=8122e4857dd204ee1d97877fb9aa44a8',
                            type: 'GET',
                            async: true
                         });

    requestWeather.done(function(data) {
        $('#pWeather').text(JSON.stringify(data));
    });

});