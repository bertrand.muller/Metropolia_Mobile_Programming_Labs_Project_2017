$(document).ready(function() {

    // Initializing array of temperatures
    var temps = [7, 2, 4, 0, -2, 5.5, -1, 1.5, 5, 3.1, 1.8];
    var min = temps[0];


    // Searching for the lowest value in array
    for(var i = 1; i < temps.length; i++) {
        if(temps[i] < min) {
            min = temps[i];
        }
    }


    // Displaying the lowest temperature
    $('#minTemp').text('minimum: ' + min);

});