$(document).ready(function() {

    // Initializing array of temperatures
    var temps = [7, 2, 4, 0, -2, 5.5, -1, 1.5, 5, 3.1, 1.8];


    // Function used to find the lowest value of an array
    var findLowest = function(arr) {

        var min = arr[0];

        for(var i = 1; i < arr.length; i++) {
            if(arr[i] < min) {
                min = arr[i];
            }
        }

        return min;

    };


    // Displaying the lowest temperature in console
    console.log(findLowest(temps));

});