$(document).ready(function() {

    // Request from server
    var requestServer = $.ajax({
                            url: 'http://users.metropolia.fi/~bertranm/MobileProgramming/Lab5/Exercise5/temperatures.txt',
                            type: 'GET',
                            async: true
                        });


    requestServer.done(function(data) {

        var dataParsed = JSON.parse(data);
        var cities = dataParsed['week9'];
        var min = cities[0]['temperatures'][0];

        // Looping on the cities
        for(var i = 0; i < cities.length; i++) {

            // Looping on the temperatures for each city
            for(var j = 0; j < cities[i]['temperatures'].length; j++) {

                var currTemp = cities[i]['temperatures'][j];

                if(currTemp < min) {
                    min = currTemp;
                }

            }
        }

        $('#minTemp').text('temperature: ' + min);

    });

});