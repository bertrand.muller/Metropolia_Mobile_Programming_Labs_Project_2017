$(document).ready(function() {

    // Variable used to know if 'h' has been pressed just before
    var hPressed = false;


    // Method used to detect keyboard events
    $(document).keyup(function(e) {

        if(e.keyCode === 72) {
            hPressed = true;
        } else if((e.keyCode === 73) && (hPressed)) {
            $('#message').text('Hello!');
        } else {
            hPressed = false;
        }

    });

});