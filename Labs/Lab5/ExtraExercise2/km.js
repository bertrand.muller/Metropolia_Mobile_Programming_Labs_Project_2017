$(document).ready(function() {


    // Hidding error message
    var span = $('span');
    span.hide();


    // Function used to calculate average speed
    var getAverageSpeed = function(d, h) {
        return (d/h).toFixed(2);
    };


    // Function used to display an arror or the average speed
    var updateDisplay = function() {

        var distance = $('#distance').val();
        var time = $('#time').val();

        if((distance >= 1) && (distance <= 10000) && (time >= 1/60) && (time <= 24)) {
            span.hide();
            $('#avSpeed').text('Average speed: ' + getAverageSpeed(distance, time) + ' km/h');
        } else {
            $('#avSpeed').text('');
            span.show();
        }

    };


    // Detecting a change in one input
    $('input').on('input', function (e) {
        updateDisplay();
    });


    // Updating display a first time
    updateDisplay();

});