$(document).on('pageinit', function() {

    var nameRecipe = localStorage.getItem('nameRecipe');
    var uri = localStorage.getItem('uri');
    $('#nRecipe').text(nameRecipe);
    displayRecipe(uri);

    $('.ui-icon-home').on('click', function(event) {
        event.preventDefault();
        window.location.href = "index.html";
    });

    $(document).bind('pagechange', function() {
        $('.ui-page-active #listRecipes').listview('refresh');
        console.log('ok page change');
    });

});


function displayRecipe(uri, name) {

    var url = '';
    if(uri) url = 'https://api.edamam.com/search?app_id=8b953f2d&app_key=d7ebaa36ea637d480f47ffbe90db9356&r=' + uri;
    else    url = 'https://api.edamam.com/search?app_id=8b953f2d&app_key=d7ebaa36ea637d480f47ffbe90db9356&q=' + name;

    var requestServer = $.ajax({
        url: url,
        type: 'GET',
        async: true,
        cache: true
    });

    requestServer.done(function(data) {

        var recipe = null;

        if(uri) recipe = data[0];
        else    recipe = data['hits'][0]['recipe'];
        var ingredients = recipe['ingredients'];

        for(var i = 0; i < ingredients.length; i++) {
            var li = '<li><span class="ing">' + ingredients[i]['text'] + '</span></li>';
            $('#listIngredients').append(li);
        }

        addInstructions(recipe);

    });

}


function addInstructions(recipe) {

    var source = $('#source');
    source.append((recipe['source'] ? recipe['source'] : '-'));

    var url = $('#url');
    url.append((recipe['url'] ? recipe['url'] : '-'));
    url.attr('href', (recipe['url'] ? recipe['url'] : '#'));

}