$(document).ready(function() {


});


function init() {

    gapi.client.setApiKey('AIzaSyCBiAFQ1Dh1ouTWOh-9MCj0HXtrNxe_sCI');
    gapi.client.load('youtube', 'v3', function() {
        searchVideos();
    });

}


function searchVideos() {

    // prepare the request
    var request = gapi.client.youtube.search.list({
        part: "snippet",
        type: "video",
        q: encodeURIComponent(localStorage.getItem('nameRecipe')).replace(/%20/g, "+"),
        maxResults: 10,
        order: "viewCount"
    });

    request.execute(function(response) {
        var results = response.result;
        $.each(results.items, function(index, item) {
            $('#results').append('<li><iframe class="video w100" width="640" height="360" src="//www.youtube.com/embed/' + item.id.videoId + '" frameborder="0" allowfullscreen></iframe></li>')
        });
    });

}