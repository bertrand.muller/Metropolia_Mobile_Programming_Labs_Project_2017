// JS file used to search for specific recipes
$(document).on('pageinit', function() {

    // Hiding the list first
    var listRecipes = $('#listRecipes');
    listRecipes.find('.ui-li-has-thumb').hide();

    displayHealthyRecipes();

    $('input').on('keyup', function(ev) {

        if(ev.keyCode === 13) {
            $('#listRecipes').find('.ui-li-has-thumb').hide();
            searchRecipes($(this).textinput().val());
        }

    });

    $('.ui-btn').on('click', function(event) {
        event.preventDefault();
        var nameRecipe = $(this).find('.nameRecipe').text();
        var uri = $(this).attr('data-value');
        console.log(uri);
        localStorage.setItem('nameRecipe', nameRecipe);
        localStorage.setItem('uri', uri.replace('#', '%23'));
        window.location.href = "recipe.html";
    });

});





function displayHealthyRecipes() {

    console.log('ok healthy');

    var from = Math.floor(Math.random() * 100);
    var to = from + 10;

    var requestServer = $.ajax({
        url: 'https://api.edamam.com/search?app_id=8b953f2d&app_key=d7ebaa36ea637d480f47ffbe90db9356&q=%20&diet=low-fat&from=' + from + '&to=' + to + '&health=alcohol-free',
        type: 'GET',
        async: true,
        cache: true
    });

    requestServer.done(function(data) {

        var hits = data['hits'];

        for(var i = 0; i < hits.length; i++) {
            var index = i + 1;
            var recipeHTML = $('#recipe' + index);
            var recipe = hits[i]['recipe'];
            recipeHTML.find('a').empty();
            addRecipeToList(recipe, index);
            recipeHTML.show();
        }

    });

}


function addRecipeToList(recipe, index) {

    if(recipe) {

        var verify = (recipe['yield'] && recipe['calories']);
        var calPerSeving = (verify ? Math.floor(recipe['calories'] / recipe['yield']) : '-');

        var img = '<img src="' + recipe['image'] + '" />' + '<span class="nameRecipe">' + recipe['label'] + '</span>';
        img += '<br>' +
            '<span class="ui-btn ui-icon-bars ui-shadow ui-corner-all ui-btn-icon-notext ui-btn-inline"></span>' +
            '<div>' + (recipe['ingredientLines'] ? recipe['ingredientLines'].length : '-') + ' ing.</div>' +
            '<span class="kcalIcon ui-btn ui-icon-heart ui-shadow ui-corner-all ui-btn-icon-notext ui-btn-inline"></span>' +
            '<div>' + calPerSeving + ' cal/u.</div>';

        var link = $('#recipe' + index).find('a');
        link.attr('data-value', recipe['uri']);
        link.append(img);

    }

}


function searchRecipes(str) {

    console.log('ok search');

    var requestServer = $.ajax({
        url: 'https://api.edamam.com/search?app_id=8b953f2d&app_key=d7ebaa36ea637d480f47ffbe90db9356&q=' + str + '&from=0&to=30',
        type: 'GET',
        async: true,
        cache: true
    });

    requestServer.done(function(data) {

        var h4 = $('h4');
        $('#listRecipes').find('.ui-li-has-thumb').hide();
        h4.empty();
        h4.append('Results for \'<i>' + str + '</i>\'');
        var hits = data['hits'];

        for(var i = 0; i < hits.length; i++) {
            var index = i + 1;
            var recipeHTML = $('#recipe' + index);
            var recipe = hits[i]['recipe'];
            recipeHTML.find('a').empty();
            addRecipeToList(recipe, index);
            recipeHTML.show();
        }

    });

}